package io.byu.todo;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button submit = (Button)findViewById(R.id.button);
        final Button button = (Button)findViewById(R.id.clearButton);
        final SharedPreferences tasks = getSharedPreferences("tasks", 0);
        final SharedPreferences.Editor taskEdit = tasks.edit();
        final EditText task = (EditText)findViewById(R.id.editText);
        final TextView text = (TextView)findViewById(R.id.textView);

        Iterator i = tasks.getAll().keySet().iterator();
        while (i.hasNext()) {
            text.setText(text.getText() + tasks.getString(i.next().toString(), "not found") + '\n');
        }


        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (task.getText().toString().trim().length() > 0) {
                    Map taskMap = tasks.getAll();
                    String key = String.valueOf(taskMap.size());
                    taskEdit.putString(key, task.getText().toString());
                    taskEdit.commit();

                    text.setText("");
                    Iterator i = tasks.getAll().keySet().iterator();
                    while (i.hasNext()) {
                        text.setText(text.getText() + tasks.getString(i.next().toString(), "not found") + '\n');
                    }
                    task.setText("");

                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Iterator i = tasks.getAll().keySet().iterator();
                while (i.hasNext()) {
                    text.setText("");
                    taskEdit.remove(i.next().toString());
                }
                taskEdit.commit();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    protected void onStop(){
//        super.onStop();
//        SharedPreferences tasks = getSharedPreferences("tasks", 0);
//        SharedPreferences.Editor editor = tasks.edit();
//        //editor.putBoolean("silentMode", mSilentMode);
//    }
}
